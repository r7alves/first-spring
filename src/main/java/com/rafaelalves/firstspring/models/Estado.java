package com.rafaelalves.firstspring.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


@Data
@Entity
@Table(name = "estados")
public class Estado implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;

    @JsonIgnore
    @OneToMany(mappedBy = "estado")
    private List<Cidade> cidades = new ArrayList<>();

    public Estado(){}

    public Estado(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

       
    
}
