package com.rafaelalves.firstspring.models.enums;

//import java.util.ArrayList;
//import java.util.Arrays;
// import java.util.List;
// import java.util.Map;
//import java.util.Optional;
// import java.util.stream.Collectors;
// import java.util.stream.Stream;

// import org.apache.commons.lang.ArrayUtils;

public enum TipoCliente {

    PESSOAFISICA(1, "Pessoa Física"),
    PESSOAJURIDICA(2, "Pessoa Jurídica");
    
    private int cod;
    private String descricao;

    private TipoCliente(int cod, String descricao){
        this.cod = cod;
        this.descricao = descricao;
    }
    
    public int getCod(){
        return cod;
    }
    
    public String getDescricao(){
        return descricao;
    }

    public static TipoCliente toEnum(Integer cod){
        if (cod == null){
            return null;
        }

        // List<TipoCliente> tipos = Arrays.asList(TipoCliente.values());

        //Optional<TipoCliente> tipos = Arrays.stream(TipoCliente.values()).filter(x -> x.cod == cod).findFirst();

        // TipoCliente tc;
        // ArrayUtils.toMap(TipoCliente.values()).forEach((k,v) -> {
        //     if(k.equals(cod) ){
        //         tc = new TipoClient
        //     }
        // });
        //if(tipos.isPresent()) return tipos.get();

        for (TipoCliente x : TipoCliente.values()) {
            if(cod.equals(x.getCod())) return x;
        }
        throw new IllegalArgumentException("Id invalido" + cod);
        

    }
}
