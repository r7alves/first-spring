package com.rafaelalves.firstspring.models;

import java.io.Serializable;

import javax.persistence.*;



import lombok.Data;

@Data
@Entity
@Table(name = "cidades")
public class Cidade implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nome;

    
    @ManyToOne
    @JoinColumn(name="estado_id")
    private Estado estado;

    public Cidade(){       
    }

    public Cidade(Integer id, String nome, Estado estado) {
        this.id = id;
        this.nome = nome;
        this.estado = estado;
    }

    

    
    
}
