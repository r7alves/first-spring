package com.rafaelalves.firstspring.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.rafaelalves.firstspring.models.Categoria;

import org.hibernate.validator.constraints.Length;

import lombok.Data;


@Data
public class CategoriaDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;


    @Length(max = 80, min = 5, message = "O tamanho deve ser entre 5 e 80 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    private String nome;

    public CategoriaDTO(){}

    public CategoriaDTO(Categoria categoria){
        id = categoria.getId();
        nome = categoria.getNome();
    }

    
}
