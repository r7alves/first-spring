package com.rafaelalves.firstspring.dto;

import java.io.Serializable;

import com.rafaelalves.firstspring.models.Produto;

import lombok.Data;


@Data
public class ProdutoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String nome;
    private Double preco;
    
    

    public ProdutoDTO(){}

    public ProdutoDTO(Produto produto){
        id = produto.getId();
        nome = produto.getNome();
        preco = produto.getPreco();    
    }

}
