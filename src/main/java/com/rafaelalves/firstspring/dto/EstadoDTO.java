package com.rafaelalves.firstspring.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.rafaelalves.firstspring.models.Estado;

import org.hibernate.validator.constraints.Length;

import lombok.Data;


@Data
public class EstadoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;

    @Length(max = 80, min = 5, message = "O tamanho deve ser entre 5 e 80 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    private String nome;

    public EstadoDTO(){}

    public EstadoDTO(Estado estado){
        id = estado.getId();
        nome = estado.getNome();
    }

    
}
