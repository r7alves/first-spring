package com.rafaelalves.firstspring.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.rafaelalves.firstspring.services.validation.ClienteInsert;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
@ClienteInsert
public class ClienteNewDTO {
    
    @Length(max = 80, min = 5, message = "O tamanho deve ser entre 5 e 80 caracteres")
    @NotEmpty(message = "Preenchimento obrigatório")
    private String nome;
    
    @NotEmpty(message = "Preenchimento obrigatório")
    @Email(message = "Email inválido")
    private String email;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String cpfOuCnpj;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String senha;
    
    private Integer tipo;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String logradouro;
    
    @NotEmpty(message = "Preenchimento obrigatório")
    private String numero;

    private String complemento;
    
    private String bairro;
    
    private String cep;

    @NotEmpty(message = "Preenchimento obrigatório")
    private String telefone1;
    
    private String telefone2;
    
    private String telefone3;

    private Integer cidadeId;

    public ClienteNewDTO(){}
    
}
