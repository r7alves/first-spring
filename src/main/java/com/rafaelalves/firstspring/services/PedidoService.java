package com.rafaelalves.firstspring.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.rafaelalves.firstspring.models.Cliente;
import com.rafaelalves.firstspring.models.ItemPedido;
//import com.rafaelalves.firstspring.models.Pagamento;
import com.rafaelalves.firstspring.models.PagamentoComBoleto;
import com.rafaelalves.firstspring.models.Pedido;
import com.rafaelalves.firstspring.models.enums.EstadoPagamento;
import com.rafaelalves.firstspring.repositories.ItemPedidoRepository;
import com.rafaelalves.firstspring.repositories.PagamentoRepository;
import com.rafaelalves.firstspring.repositories.PedidoRepository;
import com.rafaelalves.firstspring.security.UserSS;
import com.rafaelalves.firstspring.services.exceptions.AuthorizationException;
import com.rafaelalves.firstspring.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private BoletoService boletoService;

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
	private ItemPedidoRepository itemPedidoRepository;

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EmailService emailService;

    public List<Pedido> findAll() {
        return pedidoRepository.findAll();
    }

    public Pedido find(Integer id) {
        Optional<Pedido> pedido = pedidoRepository.findById(id);

        return pedido.orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName().substring(35)));
    }

    public Page<Pedido> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
        UserSS user = UserService.authenticated();

        if(user == null){
            throw new AuthorizationException("Acesso Negado");
        }

        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction) , orderBy);

        Cliente cliente = clienteService.find(user.getId());
        
        return pedidoRepository.findByCliente(cliente, pageRequest);
    }

    @Transactional
    public Pedido store(Pedido pedido){
        pedido.setId(null);
        pedido.setInstante(new Date());
        pedido.setCliente(clienteService.find(pedido.getCliente().getId()));
        pedido.getPagamento().setEstado(EstadoPagamento.PENDENTE);
        pedido.getPagamento().setPedido(pedido);

        if(pedido.getPagamento() instanceof PagamentoComBoleto){
            PagamentoComBoleto pagto = (PagamentoComBoleto) pedido.getPagamento();
            boletoService.preencherPagamentoComBoleto(pagto, pedido.getInstante());
        } 

        pedido = pedidoRepository.save(pedido);

        pagamentoRepository.save(pedido.getPagamento());

        for (ItemPedido itemPedido : pedido.getItens()) {
            itemPedido.setDesconto(0.0);
            itemPedido.setProduto(produtoService.find(itemPedido.getProduto().getId()));
            itemPedido.setPreco(itemPedido.getProduto().getPreco());
            itemPedido.setPedido(pedido);
        }

        itemPedidoRepository.saveAll(pedido.getItens());
        
        //emailService.sendOrderConfirmationEmail(pedido);
        emailService.sendOrderConfirmationHtmlEmail(pedido);

        return pedido;
    }
}
