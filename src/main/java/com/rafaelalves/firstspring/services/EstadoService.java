package com.rafaelalves.firstspring.services;

import java.util.List;

import com.rafaelalves.firstspring.models.Estado;
import com.rafaelalves.firstspring.repositories.EstadoRepository;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class EstadoService {

    @Autowired
    private EstadoRepository estadoRepository;

    public List<Estado> findAll() {
        return estadoRepository.findAllByOrderByNome();
    }
    
}
