package com.rafaelalves.firstspring.services;


import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.awt.image.BufferedImage;

import com.rafaelalves.firstspring.dto.ClienteDTO;
import com.rafaelalves.firstspring.dto.ClienteNewDTO;
import com.rafaelalves.firstspring.models.Cidade;
import com.rafaelalves.firstspring.models.Cliente;
import com.rafaelalves.firstspring.models.Endereco;
import com.rafaelalves.firstspring.models.enums.Perfil;
import com.rafaelalves.firstspring.models.enums.TipoCliente;
import com.rafaelalves.firstspring.repositories.ClienteRepository;
import com.rafaelalves.firstspring.repositories.EnderecoRepository;
import com.rafaelalves.firstspring.security.UserSS;
import com.rafaelalves.firstspring.services.exceptions.AuthorizationException;
import com.rafaelalves.firstspring.services.exceptions.DataIntegrityException;
import com.rafaelalves.firstspring.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Profile("dev")
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private BCryptPasswordEncoder passEncoder;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private S3Service s3Service;

    @Autowired
    private ImageService imageService;

    
    @Value("${img.prefix.client.profile}")
    private String prefix;
    
    @Value("${img.profile.size}")
    private Integer size;

    

    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    public Cliente find(Integer id) {
        
        UserSS user = UserService.authenticated();
        
        System.out.println(user.getId() + id);

        if(user == null || !user.hasRole(Perfil.ADMIN) && !id.equals(user.getId())){
            throw new AuthorizationException("Acesso negado");
        }
        
        Optional<Cliente> cliente = clienteRepository.findById(id);

        return cliente.orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName().substring(35)));
    }
    
    public Cliente findByEmail(String email) {
        
        UserSS user = UserService.authenticated();
        
        if(user == null || !user.hasRole(Perfil.ADMIN) && !email.equals(user.getUsername())){
            throw new AuthorizationException("Acesso negado");
        }

        Cliente cliente = clienteRepository.findByEmail(email);

         

        // return cliente.orElseThrow(() -> new ObjectNotFoundException(
        //         "Cliente não encontrado para o email: " + email)); // + ", Tipo: " + Cliente.class.getName().substring(35)));

        if (cliente == null) {
			throw new ObjectNotFoundException(
					"Cliente não encontrado! Id: " + user.getId() + ", Tipo: " + Cliente.class.getName());
		}
		return cliente;
    }

    @Transactional
    public Cliente store(Cliente cliente) {

        if (cliente.getId() == null) {
            cliente.setId(null);
        } else {
            find(cliente.getId());
        }

        cliente = clienteRepository.save(cliente);
        
        enderecoRepository.saveAll((cliente.getEnderecos()));

        return cliente;
    }

    public Cliente update(Cliente cliente) {
        Cliente cliente2 =  find(cliente.getId());
        cliente2.setNome(cliente.getNome());
        cliente2.setEmail(cliente.getEmail());
        return clienteRepository.save(cliente2);
    }

    public void delete(Integer id) {
        find(id);
        try {
            clienteRepository.deleteById(id);
        } catch (org.springframework.dao.DataIntegrityViolationException e) {
            throw new DataIntegrityException("Não foi possivel realizar a ação");
        }
    }

    public Page<Cliente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction) , orderBy);

        return clienteRepository.findAll(pageRequest);
    }

    public Cliente fromDTO(ClienteDTO clienteDTO){
        return new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getEmail(), null, null, null);
    } 

    public Cliente fromDTO(ClienteNewDTO obj){
        Cliente cliente = new Cliente(null, obj.getNome(), obj.getEmail(), obj.getCpfOuCnpj(), TipoCliente.toEnum(obj.getTipo()), passEncoder.encode(obj.getSenha()));
        
        Cidade cidade = new Cidade(obj.getCidadeId(), null, null);
        
        Endereco endereco = new Endereco(null, obj.getLogradouro(), obj.getNumero(), obj.getComplemento(), obj.getBairro(), obj.getCep(), cliente, cidade);
        
        cliente.getEnderecos().add(endereco);
        
        cliente.getTelefones().add(obj.getTelefone1());
        
        if(obj.getTelefone2() == null){
            cliente.getTelefones().add(obj.getTelefone2());

        }
        if(obj.getTelefone3() == null){
            cliente.getTelefones().add(obj.getTelefone2());

        }

        return cliente;

    } 

    public URI uploadProfilePicture(MultipartFile multipartFile){
        UserSS user = UserService.authenticated();
        if (user == null){
            throw new AuthorizationException("Acesso negado");
        }

        BufferedImage jpgImage = imageService.getJpgImageFromFile(multipartFile);

        jpgImage = imageService.cropSquare(jpgImage);

        jpgImage = imageService.resize(jpgImage, size);

        String fileName = prefix + user.getId() + ".jpg";

        return s3Service.uploadFile(imageService.getInputStream(jpgImage, "jpg"), fileName, "image");
      
    }
}
