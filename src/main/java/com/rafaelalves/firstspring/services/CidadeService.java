package com.rafaelalves.firstspring.services;

import java.util.List;

import com.rafaelalves.firstspring.models.Cidade;
import com.rafaelalves.firstspring.repositories.CidadeRepository;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository cidadeRepository;

    public List<Cidade> findByEstado(Integer estadoId) {
        return cidadeRepository.findCidades(estadoId);
    }
    
}
