package com.rafaelalves.firstspring.services.validation;

import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.rafaelalves.firstspring.dto.ClienteNewDTO;
import com.rafaelalves.firstspring.models.Cliente;
import com.rafaelalves.firstspring.models.enums.TipoCliente;
import com.rafaelalves.firstspring.repositories.ClienteRepository;
import com.rafaelalves.firstspring.resources.exceptions.FieldMessage;
import com.rafaelalves.firstspring.services.validation.utils.BR;

import org.springframework.beans.factory.annotation.Autowired;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, ClienteNewDTO> {
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Override
    public void initialize(ClienteInsert ann) {
    }

    @Override
    public boolean isValid(ClienteNewDTO objDto, ConstraintValidatorContext context) {
        List<FieldMessage> list = new ArrayList<>();

        Cliente clienteEmail = clienteRepository.findByEmail(objDto.getEmail());

        if(clienteEmail != null) list.add(new FieldMessage("email", "Email já existe"));
        
        Cliente clienteCpf = clienteRepository.findByCpfOuCnpj(objDto.getCpfOuCnpj());

        if(clienteCpf != null) list.add(new FieldMessage("cpfOuCnpj", "CpfOuCnpj já existe"));



        if(objDto.getTipo().equals(TipoCliente.PESSOAFISICA.getCod()) && !BR.isValidCPF(objDto.getCpfOuCnpj())){
            list.add(new FieldMessage("cpfOuCnpj", "CPF inválido"));
        }
        if(objDto.getTipo().equals(TipoCliente.PESSOAJURIDICA.getCod()) && !BR.isValidCNPJ(objDto.getCpfOuCnpj())){
            list.add(new FieldMessage("cpfOuCnpj", "CNPJ inválido"));
        }

        

        for (FieldMessage e : list) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        }
        return list.isEmpty();
    }
}
