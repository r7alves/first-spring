package com.rafaelalves.firstspring.services;

import java.util.List;
import java.util.Optional;

import com.rafaelalves.firstspring.dto.CategoriaDTO;
import com.rafaelalves.firstspring.models.Categoria;
import com.rafaelalves.firstspring.repositories.CategoriaRepository;
import com.rafaelalves.firstspring.services.exceptions.ObjectNotFoundException;
import com.rafaelalves.firstspring.services.exceptions.DataIntegrityException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    public List<Categoria> findAll() {
        return categoriaRepository.findAll();
    }
    public Categoria find(Integer id) {
        Optional<Categoria> categoria = categoriaRepository.findById(id);

        return categoria.orElseThrow(() -> new ObjectNotFoundException(
                "Objeto não encontrado! Id: " + id + ", Tipo: " + Categoria.class.getName().substring(35)));
    }

    public Categoria store(Categoria categoria) {

        if (categoria.getId() == null) {
            categoria.setId(null);
        } else {
            find(categoria.getId());
        }

        return categoriaRepository.save(categoria);
    }

    public Categoria update(Categoria categoria) {
        find(categoria.getId());
        return categoriaRepository.save(categoria);
    }

    public void delete(Integer id) {
        find(id);
        try {
            categoriaRepository.deleteById(id);
        } catch (org.springframework.dao.DataIntegrityViolationException e) {
            throw new DataIntegrityException("Não foi possivel realizar a ação");
        }
    }

    public Page<Categoria> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
        // categorias = Categoria::orderBy('id', 'desc')->paginate(10)
        PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction) , orderBy);

        return categoriaRepository.findAll(pageRequest);
    }

    public Categoria fromDTO(CategoriaDTO categoriaDTO){
       return new Categoria(categoriaDTO.getId(), categoriaDTO.getNome()); 
    } 
}
