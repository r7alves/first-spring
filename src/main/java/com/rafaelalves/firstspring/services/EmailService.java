package com.rafaelalves.firstspring.services;

import javax.mail.internet.MimeMessage;

import com.rafaelalves.firstspring.models.Cliente;
import com.rafaelalves.firstspring.models.Pedido;

import org.springframework.mail.SimpleMailMessage;

public interface EmailService {
    
    void sendOrderConfirmationEmail(Pedido pedido);

    void sendEmail(SimpleMailMessage msg);

    void sendOrderConfirmationHtmlEmail(Pedido pedido);

    void sendHtmlEmail(MimeMessage msg);

    void sendNewPasswordEmail(Cliente cliente, String newPass);
}
