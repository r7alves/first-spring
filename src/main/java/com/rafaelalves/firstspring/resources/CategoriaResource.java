package com.rafaelalves.firstspring.resources;

import java.net.URI;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.rafaelalves.firstspring.dto.CategoriaDTO;
import com.rafaelalves.firstspring.models.Categoria;
import com.rafaelalves.firstspring.services.CategoriaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;





@RestController
@RequestMapping(value = "/categorias")
public class CategoriaResource {

    @Autowired
    private CategoriaService categoriaService;

    @RequestMapping(method = RequestMethod.GET)
    public  ResponseEntity<List<CategoriaDTO>> findAll() {

        List<Categoria> categorias = categoriaService.findAll();

        List<CategoriaDTO> categoriaDTOs = categorias.stream().map(categoria ->  new CategoriaDTO(categoria)).collect(Collectors.toList());

        return ResponseEntity.ok().body(categoriaDTOs);
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public  ResponseEntity<Page<CategoriaDTO>> findPaginate(
        @RequestParam(value = "page", defaultValue = "0") Integer page, 
        @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage, 
        @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy, 
        @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {

        Page<Categoria> categorias = categoriaService.findPage(page, linesPerPage, orderBy, direction);

        Page<CategoriaDTO> categoriaDTOs = categorias.map(categoria ->  new CategoriaDTO(categoria));

        return ResponseEntity.ok().body(categoriaDTOs);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Categoria> find(@PathVariable Integer id) {
        Categoria categoria = categoriaService.find(id);
        System.out.println("============================="+categoria);
        return ResponseEntity.ok().body(categoria);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> store(@Valid @RequestBody CategoriaDTO categoriaDTO) {
        Categoria categoria = categoriaService.fromDTO(categoriaDTO);
        
        categoria = categoriaService.store(categoria);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(categoria.getId())
                .toUri();
        return ResponseEntity.created(uri).build();

    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody CategoriaDTO categoriaDTO, @PathVariable Integer id )  {
        Categoria categoria = categoriaService.fromDTO(categoriaDTO);
        
        categoria.setId(id);
        categoria = categoriaService.store(categoria);

        return ResponseEntity.noContent().build();

    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        categoriaService.delete(id);
        return ResponseEntity.noContent().build();
    }
    
    

}
