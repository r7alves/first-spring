package com.rafaelalves.firstspring.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import com.rafaelalves.firstspring.models.Pedido;
import com.rafaelalves.firstspring.services.PedidoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;




@RestController
@RequestMapping(value = "/pedidos")
public class PedidoResource {

    @Autowired
    private  PedidoService pedidoService;

    @RequestMapping(method = RequestMethod.GET)
    public  ResponseEntity<List<Pedido>> findAll() {

        List<Pedido> pedidos = pedidoService.findAll();

        return ResponseEntity.ok().body(pedidos);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Pedido> find(@PathVariable Integer id) {
        Pedido pedido = pedidoService.find(id);
        
        return ResponseEntity.ok().body(pedido);
    }
    
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public  ResponseEntity<Page<Pedido>> findPaginate(
        @RequestParam(value = "page", defaultValue = "0") Integer page, 
        @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage, 
        @RequestParam(value = "orderBy", defaultValue = "instante") String orderBy, 
        @RequestParam(value = "direction", defaultValue = "DESC") String direction
    ) {

        Page<Pedido> pedidos = pedidoService.findPage(page, linesPerPage, orderBy, direction);

        return ResponseEntity.ok().body(pedidos);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> store(@Valid @RequestBody Pedido pedido) {
        
        
        pedido = pedidoService.store(pedido);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pedido.getId())
                .toUri();
        return ResponseEntity.created(uri).build();

    }
    
    
}
