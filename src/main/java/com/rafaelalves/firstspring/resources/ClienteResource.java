package com.rafaelalves.firstspring.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

//import javax.mail.Multipart;
import javax.validation.Valid;

import com.rafaelalves.firstspring.dto.ClienteDTO;
import com.rafaelalves.firstspring.dto.ClienteNewDTO;
import com.rafaelalves.firstspring.models.Cliente;
import com.rafaelalves.firstspring.services.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteResource {

    @Autowired
    private  ClienteService clienteService;

   @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Cliente> find(@PathVariable Integer id) {
        Cliente cliente = clienteService.find(id);
        
        return ResponseEntity.ok().body(cliente);
    }
   
    @RequestMapping(value="/email", method=RequestMethod.GET)
    public ResponseEntity<Cliente> find(@RequestParam(value = "value") String email) {
        Cliente cliente = clienteService.findByEmail(email);
        
        return ResponseEntity.ok().body(cliente);
    }


    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(method = RequestMethod.GET)
    public  ResponseEntity<List<ClienteDTO>> findAll() {

        List<Cliente> clientes = clienteService.findAll();

        List<ClienteDTO> clienteDTOs = clientes.stream().map(cliente ->  new ClienteDTO(cliente)).collect(Collectors.toList());

        return ResponseEntity.ok().body(clienteDTOs);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public  ResponseEntity<Page<ClienteDTO>> findPaginate(
        @RequestParam(value = "page", defaultValue = "0") Integer page, 
        @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage, 
        @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy, 
        @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {

        Page<Cliente> clientes = clienteService.findPage(page, linesPerPage, orderBy, direction);

        Page<ClienteDTO> clienteDTOs = clientes.map(cliente ->  new ClienteDTO(cliente));

        return ResponseEntity.ok().body(clienteDTOs);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> store(@Valid @RequestBody ClienteNewDTO clienteNewDTO) {
        Cliente cliente = clienteService.fromDTO(clienteNewDTO);
        
        cliente = clienteService.store(cliente);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cliente.getId())
                .toUri();
        return ResponseEntity.created(uri).build();

    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody ClienteDTO clienteDTO, @PathVariable Integer id )  {
        Cliente cliente = clienteService.fromDTO(clienteDTO);
        
        cliente.setId(id);
        cliente = clienteService.update(cliente);

        return ResponseEntity.noContent().build();

    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        clienteService.delete(id);
        return ResponseEntity.noContent().build();
    }
    
    @RequestMapping(value = "/picture", method = RequestMethod.POST)
    public ResponseEntity<Void> uploadProfilePicture(@RequestParam(name = "file") MultipartFile file) {
        
        URI uri = clienteService.uploadProfilePicture(file);
        
        return ResponseEntity.created(uri).build();

    }

}
