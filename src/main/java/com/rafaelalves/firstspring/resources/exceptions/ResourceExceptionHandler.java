package com.rafaelalves.firstspring.resources.exceptions;

import javax.servlet.http.HttpServletRequest;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.rafaelalves.firstspring.services.exceptions.AuthorizationException;
import com.rafaelalves.firstspring.services.exceptions.DataIntegrityException;
import com.rafaelalves.firstspring.services.exceptions.FileException;
import com.rafaelalves.firstspring.services.exceptions.ObjectNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<StardardError> objectNotFound(ObjectNotFoundException e, HttpServletRequest request){
        StardardError err = new StardardError(System.currentTimeMillis(), HttpStatus.NOT_FOUND.value(), "Não encontrado", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @ExceptionHandler(DataIntegrityException.class)
    public ResponseEntity<StardardError> dataIntegrity(DataIntegrityException e, HttpServletRequest request){
        StardardError err = new StardardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Integridade de dados", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<StardardError> validation(MethodArgumentNotValidException e, HttpServletRequest request){
        
        ValidationError err = new ValidationError(System.currentTimeMillis(), HttpStatus.UNPROCESSABLE_ENTITY.value(), "Erro de validção", e.getMessage(), request.getRequestURI());
        
        for (FieldError x : e.getBindingResult().getFieldErrors()) {
            err.addError(x.getField(), x.getDefaultMessage());
        }
        
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
    }

    @ExceptionHandler(AuthorizationException.class)
    public ResponseEntity<StardardError> authorization(AuthorizationException e, HttpServletRequest request){
        StardardError err = new StardardError(System.currentTimeMillis(), HttpStatus.FORBIDDEN.value(), "Acesso negado", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @ExceptionHandler(FileException.class)
    public ResponseEntity<StardardError> file(FileException e, HttpServletRequest request){
        StardardError err =  new StardardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Erro de arquivo", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(AmazonServiceException.class)
    public ResponseEntity<StardardError> amazonService(AmazonServiceException e, HttpServletRequest request){

        HttpStatus code = HttpStatus.valueOf(e.getErrorCode());
        StardardError err = new StardardError(System.currentTimeMillis(), code.value(), "Erro AmazonService", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(code).body(err);
    }


    @ExceptionHandler(AmazonClientException.class)
    public ResponseEntity<StardardError> amazonClient(AmazonClientException e, HttpServletRequest request){
        StardardError err = new StardardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Erro AmazonCliente", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(AmazonS3Exception.class)
    public ResponseEntity<StardardError> amazonS3(AmazonS3Exception e, HttpServletRequest request){
        StardardError err = new StardardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(), "Erro S3", e.getMessage(), request.getRequestURI());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    
    
}
