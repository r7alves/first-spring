package com.rafaelalves.firstspring.resources;

import java.util.List;

import com.rafaelalves.firstspring.dto.ProdutoDTO;
import com.rafaelalves.firstspring.models.Produto;
import com.rafaelalves.firstspring.resources.utils.URL;
import com.rafaelalves.firstspring.services.ProdutoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;




@RestController
@RequestMapping(value = "/produtos")
public class ProdutoResource {

    @Autowired
    private  ProdutoService produtoService;

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Produto> find(@PathVariable Integer id) {
        Produto produto = produtoService.find(id);
        
        return ResponseEntity.ok().body(produto);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public  ResponseEntity<Page<ProdutoDTO>> findPaginate(
        @RequestParam(value = "nome", defaultValue = "") String nome, 
        @RequestParam(value = "categorias", defaultValue = "") String categorias, 
        @RequestParam(value = "page", defaultValue = "0") Integer page, 
        @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage, 
        @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy, 
        @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {
        List<Integer> ids = URL.decodeIntList(categorias);

        String nomeDecoded = URL.decodeParam(nome);

        Page<Produto> produtos = produtoService.search(nomeDecoded, ids,page, linesPerPage, orderBy, direction);

        Page<ProdutoDTO> produtoDTOs = produtos.map(produto ->  new ProdutoDTO(produto));

        return ResponseEntity.ok().body(produtoDTOs);
    }
    
}
