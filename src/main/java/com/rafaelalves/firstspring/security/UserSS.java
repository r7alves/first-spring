package com.rafaelalves.firstspring.security;

import java.util.Collection;
//import java.util.Set;
import java.util.stream.Collectors;

import com.rafaelalves.firstspring.models.Cliente;
//import com.rafaelalves.firstspring.models.enums.Perfil;
import com.rafaelalves.firstspring.models.enums.Perfil;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserSS implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String email;
    private String senha;
    private Collection<? extends GrantedAuthority> authorities; 


    public UserSS(){}

    

    public Integer getId() {
        return id;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        
        return true;
    }

    @Override
    public boolean isEnabled() {
        
        return true;
    }

    public UserSS(Cliente cliente) {
        this.id = cliente.getId();
        this.email = cliente.getEmail();
        this.senha = cliente.getSenha();
        this.authorities = cliente.getPerfis().stream().map(p -> new SimpleGrantedAuthority(p.getDescricao())).collect(Collectors.toList());
    }

    public boolean hasRole(Perfil perfil){
        return getAuthorities().contains(new SimpleGrantedAuthority(perfil.getDescricao()));
    }
    
}
